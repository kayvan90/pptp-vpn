# PPTP server on cetos 7
this is instruction which describes how to setup a pptp server on your VPS, (cetos 7)

For this instructions we assume your virtual server has IP: 90.XXX.XXX.XXX (replace with the real IP everywhere).

### Then login via SSH via a 'user' who has root access 
```
ssh user@90.XXX.XXX.XXX
```

### Following these commands and instructions, update your VPS:

```
sudo yum update 
sudo yum upgrade 
sudo yum install pptpd
```

### Edit the file /etc/pptpd.conf and add:

```
sudo vim /etc/pptpd.conf (press enter and and the following lines to the file)
localip 192.168.0.1 
remoteip 192.168.0.2-254
```

### Insert the dns servers in /etc/ppp/pptpd-options:

```
sudo vim /etc/ppp/pptpd-options (add this lines:)
ms-dns 8.8.8.8
ms-dns 8.8.4.4
```

### Create two test users in /etc/ppp/chap-secrets

```
sudo vim /etc/ppp/chap-secrets
user1 pptpd password1 * 
user2 pptpd password2 *
```

### Setup routing for VPN server in /etc/sysctl.conf:

```
sudo /etc/sysctl.conf
net.ipv4.ip_forward=1
```


### add firewall rules:
```
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 0 -p gre -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv6 filter INPUT 0 -p gre -j ACCEPT
firewall-cmd --reload
```


### In case it didn't worked, try the fowllowing iptables (change your interface accordingly to eth0, ensXX or ...)
```
iptables -A INPUT -i ens160 -p tcp --dport 1723 -j ACCEPT
iptables -A INPUT -i ens160 -p gre -j ACCEPT
iptables -t nat -A POSTROUTING -o ens160 -j MASQUERADE
iptables -A FORWARD -i ppp+ -o ens160 -j ACCEPT
iptables -A FORWARD -i ens160 -o ppp+ -j ACCEPT
service iptables save
service iptables restart
```

### restart pptp service
```
service pptpd restart
```
